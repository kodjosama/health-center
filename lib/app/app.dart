import 'package:hc/services/api/health_center_api_service.dart';
import 'package:hc/services/connectivity_service.dart';
import 'package:hc/services/db/health_center_db_service.dart';
import 'package:hc/services/env_service.dart';
import 'package:hc/ui/views/health_center/details/health_center_details_view.dart';
import 'package:hc/ui/views/health_center/form/health_center_form_view.dart';
import 'package:hc/ui/views/health_center/list/health_center_list_view.dart';
import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';

@StackedApp(
  routes: [
    MaterialRoute(page: HealthCenterListView, initial: true),
    MaterialRoute(page: HealthCenterFormView),
    MaterialRoute(page: HealthCenterDetailsView),
  ],
  dependencies: [
    LazySingleton(classType: NavigationService),
    LazySingleton(classType: DialogService),
    LazySingleton(classType: SnackbarService),
    InitializableSingleton(classType: EnvService),
    LazySingleton(classType: HealthCenterApiService),
    InitializableSingleton(classType: HealthCenterDbService),
    LazySingleton(classType: ConnectivityService),
  ],
)
class App {}
