// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedNavigatorGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:flutter/material.dart' as _i5;
import 'package:flutter/material.dart';
import 'package:hc/models/health_center.dart' as _i6;
import 'package:hc/ui/views/health_center/details/health_center_details_view.dart'
    as _i4;
import 'package:hc/ui/views/health_center/form/health_center_form_view.dart'
    as _i3;
import 'package:hc/ui/views/health_center/list/health_center_list_view.dart'
    as _i2;
import 'package:stacked/stacked.dart' as _i1;
import 'package:stacked_services/stacked_services.dart' as _i7;

class Routes {
  static const healthCenterListView = '/';

  static const healthCenterFormView = '/health-center-form-view';

  static const healthCenterDetailsView = '/health-center-details-view';

  static const all = <String>{
    healthCenterListView,
    healthCenterFormView,
    healthCenterDetailsView,
  };
}

class StackedRouter extends _i1.RouterBase {
  final _routes = <_i1.RouteDef>[
    _i1.RouteDef(
      Routes.healthCenterListView,
      page: _i2.HealthCenterListView,
    ),
    _i1.RouteDef(
      Routes.healthCenterFormView,
      page: _i3.HealthCenterFormView,
    ),
    _i1.RouteDef(
      Routes.healthCenterDetailsView,
      page: _i4.HealthCenterDetailsView,
    ),
  ];

  final _pagesMap = <Type, _i1.StackedRouteFactory>{
    _i2.HealthCenterListView: (data) {
      return _i5.MaterialPageRoute<dynamic>(
        builder: (context) => const _i2.HealthCenterListView(),
        settings: data,
      );
    },
    _i3.HealthCenterFormView: (data) {
      final args = data.getArgs<HealthCenterFormViewArguments>(
        orElse: () => const HealthCenterFormViewArguments(),
      );
      return _i5.MaterialPageRoute<dynamic>(
        builder: (context) => _i3.HealthCenterFormView(
            key: args.key, healthCenter: args.healthCenter),
        settings: data,
      );
    },
    _i4.HealthCenterDetailsView: (data) {
      final args =
          data.getArgs<HealthCenterDetailsViewArguments>(nullOk: false);
      return _i5.MaterialPageRoute<dynamic>(
        builder: (context) => _i4.HealthCenterDetailsView(
            key: args.key, healthCenter: args.healthCenter),
        settings: data,
      );
    },
  };

  @override
  List<_i1.RouteDef> get routes => _routes;
  @override
  Map<Type, _i1.StackedRouteFactory> get pagesMap => _pagesMap;
}

class HealthCenterFormViewArguments {
  const HealthCenterFormViewArguments({
    this.key,
    this.healthCenter,
  });

  final _i5.Key? key;

  final _i6.HealthCenter? healthCenter;

  @override
  String toString() {
    return '{"key": "$key", "healthCenter": "$healthCenter"}';
  }

  @override
  bool operator ==(covariant HealthCenterFormViewArguments other) {
    if (identical(this, other)) return true;
    return other.key == key && other.healthCenter == healthCenter;
  }

  @override
  int get hashCode {
    return key.hashCode ^ healthCenter.hashCode;
  }
}

class HealthCenterDetailsViewArguments {
  const HealthCenterDetailsViewArguments({
    this.key,
    required this.healthCenter,
  });

  final _i5.Key? key;

  final _i6.HealthCenter healthCenter;

  @override
  String toString() {
    return '{"key": "$key", "healthCenter": "$healthCenter"}';
  }

  @override
  bool operator ==(covariant HealthCenterDetailsViewArguments other) {
    if (identical(this, other)) return true;
    return other.key == key && other.healthCenter == healthCenter;
  }

  @override
  int get hashCode {
    return key.hashCode ^ healthCenter.hashCode;
  }
}

extension NavigatorStateExtension on _i7.NavigationService {
  Future<dynamic> navigateToHealthCenterListView([
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  ]) async {
    return navigateTo<dynamic>(Routes.healthCenterListView,
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> navigateToHealthCenterFormView({
    _i5.Key? key,
    _i6.HealthCenter? healthCenter,
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  }) async {
    return navigateTo<dynamic>(Routes.healthCenterFormView,
        arguments:
            HealthCenterFormViewArguments(key: key, healthCenter: healthCenter),
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> navigateToHealthCenterDetailsView({
    _i5.Key? key,
    required _i6.HealthCenter healthCenter,
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  }) async {
    return navigateTo<dynamic>(Routes.healthCenterDetailsView,
        arguments: HealthCenterDetailsViewArguments(
            key: key, healthCenter: healthCenter),
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> replaceWithHealthCenterListView([
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  ]) async {
    return replaceWith<dynamic>(Routes.healthCenterListView,
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> replaceWithHealthCenterFormView({
    _i5.Key? key,
    _i6.HealthCenter? healthCenter,
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  }) async {
    return replaceWith<dynamic>(Routes.healthCenterFormView,
        arguments:
            HealthCenterFormViewArguments(key: key, healthCenter: healthCenter),
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }

  Future<dynamic> replaceWithHealthCenterDetailsView({
    _i5.Key? key,
    required _i6.HealthCenter healthCenter,
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  }) async {
    return replaceWith<dynamic>(Routes.healthCenterDetailsView,
        arguments: HealthCenterDetailsViewArguments(
            key: key, healthCenter: healthCenter),
        id: routerId,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
        transition: transition);
  }
}
