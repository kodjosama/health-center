// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedLocatorGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs, implementation_imports, depend_on_referenced_packages

import 'package:stacked_services/src/dialog/dialog_service.dart';
import 'package:stacked_services/src/navigation/navigation_service.dart';
import 'package:stacked_services/src/snackbar/snackbar_service.dart';
import 'package:stacked_shared/stacked_shared.dart';

import '../services/api/health_center_api_service.dart';
import '../services/connectivity_service.dart';
import '../services/db/health_center_db_service.dart';
import '../services/env_service.dart';

final locator = StackedLocator.instance;

Future<void> setupLocator({
  String? environment,
  EnvironmentFilter? environmentFilter,
}) async {
// Register environments
  locator.registerEnvironment(
      environment: environment, environmentFilter: environmentFilter);

// Register dependencies
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => SnackbarService());
  final envService = EnvService();
  await envService.init();
  locator.registerSingleton(envService);

  locator.registerLazySingleton(() => HealthCenterApiService());
  final healthCenterDbService = HealthCenterDbService();
  await healthCenterDbService.init();
  locator.registerSingleton(healthCenterDbService);

  locator.registerLazySingleton(() => ConnectivityService());
}
