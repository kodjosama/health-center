// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'health_center.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

HealthCenter _$HealthCenterFromJson(Map<String, dynamic> json) {
  return _HealthCenter.fromJson(json);
}

/// @nodoc
mixin _$HealthCenter {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get phoneNumber => throw _privateConstructorUsedError;
  String get city => throw _privateConstructorUsedError;
  String get address => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HealthCenterCopyWith<HealthCenter> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HealthCenterCopyWith<$Res> {
  factory $HealthCenterCopyWith(
          HealthCenter value, $Res Function(HealthCenter) then) =
      _$HealthCenterCopyWithImpl<$Res, HealthCenter>;
  @useResult
  $Res call(
      {int id, String name, String phoneNumber, String city, String address});
}

/// @nodoc
class _$HealthCenterCopyWithImpl<$Res, $Val extends HealthCenter>
    implements $HealthCenterCopyWith<$Res> {
  _$HealthCenterCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? phoneNumber = null,
    Object? city = null,
    Object? address = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      city: null == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_HealthCenterCopyWith<$Res>
    implements $HealthCenterCopyWith<$Res> {
  factory _$$_HealthCenterCopyWith(
          _$_HealthCenter value, $Res Function(_$_HealthCenter) then) =
      __$$_HealthCenterCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id, String name, String phoneNumber, String city, String address});
}

/// @nodoc
class __$$_HealthCenterCopyWithImpl<$Res>
    extends _$HealthCenterCopyWithImpl<$Res, _$_HealthCenter>
    implements _$$_HealthCenterCopyWith<$Res> {
  __$$_HealthCenterCopyWithImpl(
      _$_HealthCenter _value, $Res Function(_$_HealthCenter) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? phoneNumber = null,
    Object? city = null,
    Object? address = null,
  }) {
    return _then(_$_HealthCenter(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      phoneNumber: null == phoneNumber
          ? _value.phoneNumber
          : phoneNumber // ignore: cast_nullable_to_non_nullable
              as String,
      city: null == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String,
      address: null == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_HealthCenter with DiagnosticableTreeMixin implements _HealthCenter {
  const _$_HealthCenter(
      {required this.id,
      required this.name,
      required this.phoneNumber,
      required this.city,
      required this.address});

  factory _$_HealthCenter.fromJson(Map<String, dynamic> json) =>
      _$$_HealthCenterFromJson(json);

  @override
  final int id;
  @override
  final String name;
  @override
  final String phoneNumber;
  @override
  final String city;
  @override
  final String address;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'HealthCenter(id: $id, name: $name, phoneNumber: $phoneNumber, city: $city, address: $address)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'HealthCenter'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('phoneNumber', phoneNumber))
      ..add(DiagnosticsProperty('city', city))
      ..add(DiagnosticsProperty('address', address));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_HealthCenter &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.phoneNumber, phoneNumber) ||
                other.phoneNumber == phoneNumber) &&
            (identical(other.city, city) || other.city == city) &&
            (identical(other.address, address) || other.address == address));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, name, phoneNumber, city, address);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_HealthCenterCopyWith<_$_HealthCenter> get copyWith =>
      __$$_HealthCenterCopyWithImpl<_$_HealthCenter>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_HealthCenterToJson(
      this,
    );
  }
}

abstract class _HealthCenter implements HealthCenter {
  const factory _HealthCenter(
      {required final int id,
      required final String name,
      required final String phoneNumber,
      required final String city,
      required final String address}) = _$_HealthCenter;

  factory _HealthCenter.fromJson(Map<String, dynamic> json) =
      _$_HealthCenter.fromJson;

  @override
  int get id;
  @override
  String get name;
  @override
  String get phoneNumber;
  @override
  String get city;
  @override
  String get address;
  @override
  @JsonKey(ignore: true)
  _$$_HealthCenterCopyWith<_$_HealthCenter> get copyWith =>
      throw _privateConstructorUsedError;
}
