// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'health_center.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_HealthCenter _$$_HealthCenterFromJson(Map<String, dynamic> json) =>
    _$_HealthCenter(
      id: json['id'] as int,
      name: json['name'] as String,
      phoneNumber: json['phone_number'] as String,
      city: json['city'] as String,
      address: json['address'] as String,
    );

Map<String, dynamic> _$$_HealthCenterToJson(_$_HealthCenter instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'phone_number': instance.phoneNumber,
      'city': instance.city,
      'address': instance.address,
    };
