import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'health_center.freezed.dart';

part 'health_center.g.dart';

@freezed
class HealthCenter with _$HealthCenter {
  const factory HealthCenter({
    required int id,
    required String name,
    required String phoneNumber,
    required String city,
    required String address,
  }) = _HealthCenter;

  factory HealthCenter.fromJson(Map<String, dynamic> json) =>
      _$HealthCenterFromJson(json);
}
