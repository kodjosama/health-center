import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hc/ui/viewmodels/connectivity_viewmodel.dart';
import 'package:provider/provider.dart';
import 'package:stacked_services/stacked_services.dart';

import 'app/app.locator.dart';
import 'app/app.router.dart';
import 'ui/setups/setup_dialog_ui.dart';
import 'ui/setups/setup_snackbar_ui.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await setupLocator();

  setupDialogUi();

  setupSnackbarUi();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ConnectivityViewModel()),
      ],
      child: Consumer<ConnectivityViewModel>(
        builder: (context, connectivityViewModel, child) {
          debugPrint('connectivity view model ${connectivityViewModel.state}');

          return MaterialApp(
            navigatorKey: StackedService.navigatorKey,
            onGenerateRoute: StackedRouter().onGenerateRoute,
            navigatorObservers: [StackedService.routeObserver],
            title: 'Health Center',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
              textTheme: GoogleFonts.latoTextTheme(Theme.of(context).textTheme),
            ),
          );
        },
      ),
    );
  }
}
