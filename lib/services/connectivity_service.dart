import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hc/app/app.locator.dart';
import 'package:hc/services/api/health_center_api_service.dart';
import 'package:hc/services/db/health_center_db_service.dart';
import 'package:stacked/stacked.dart';

class ConnectivityService with ListenableServiceMixin {
  final ReactiveValue<ConnectivityResult> _rvState =
      ReactiveValue(ConnectivityResult.none);

  ConnectivityService() {
    listenToReactiveValues([_rvState]);

    _rvState.onChange.listen((change) {
      if (change.old != change.neu) {
        sync();
      }
    });
  }

  ConnectivityResult get state => _rvState.value;

  set state(ConnectivityResult state) {
    _rvState.value = state;
  }

  Future<void> sync() async {
    debugPrint('syncing data with connectivity result $state');

    if (state != ConnectivityResult.none) {
      final dbService = locator<HealthCenterDbService>();
      final apiService = locator<HealthCenterApiService>();

      final list = await dbService.get();

      for (var e in list) {
        await apiService.update(
          e.id,
          FormData.fromMap({
            ...e.toJson()..remove('id'),
            '_method': 'PUT',
          }),
        );

        await dbService.delete(e.id);
      }
    }
  }
}
