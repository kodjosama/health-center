import 'package:dio/dio.dart';
import 'package:hc/app/app.locator.dart';
import 'package:hc/services/env_service.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

abstract class ApiService {
  String get url => locator<EnvService>().url;

  Dio get request {
    return Dio(
      BaseOptions(
        baseUrl: '$url/',
        headers: {'Accept': 'application/json'},
      ),
    )..interceptors.add(PrettyDioLogger());
  }
}
