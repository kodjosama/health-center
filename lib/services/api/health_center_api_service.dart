import 'package:dio/dio.dart';
import 'package:hc/models/health_center.dart';

import 'api_service.dart';

class HealthCenterApiService extends ApiService {
  String get _path => 'health-centers';

  Future<List<HealthCenter>> get() {
    return request.get<Map>(_path).then((response) {
      return (response.data!['data'] as List)
          .map((e) => HealthCenter.fromJson(e))
          .toList();
    });
  }

  Future<void> create(FormData data) async {
    await request.post(_path, data: data);
  }

  Future<void> update(int id, FormData data) async {
    await request.post('$_path/$id', data: data);
  }
}
