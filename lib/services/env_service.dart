import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:stacked/stacked_annotations.dart';

class EnvService implements InitializableDependency {
  String get url => get('APP_URL')!;

  String? get(String key) => dotenv.env[key];

  @override
  Future<void> init() => dotenv.load(fileName: '.env');
}
