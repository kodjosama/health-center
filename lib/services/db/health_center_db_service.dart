import 'package:flutter/foundation.dart';
import 'package:hc/app/app.locator.dart';
import 'package:hc/models/health_center.dart';
import 'package:hc/services/connectivity_service.dart';
import 'package:path/path.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:sqflite_common_ffi_web/sqflite_ffi_web.dart';
import 'package:stacked/stacked_annotations.dart';

class HealthCenterDbService implements InitializableDependency {
  late Database db;

  Future<void> create(Map<String, dynamic> data) async {
    await db.transaction((txn) async {
      await txn.insert(
        'health_centers',
        data,
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }).then((_) {
      debugPrint('saved to db completed');

      locator<ConnectivityService>().sync();
    });
  }

  Future<void> delete(int id) async {
    await db.transaction((txn) async {
      await txn.delete(
        'health_centers',
        where: 'id = ?',
        whereArgs: [id],
      );
    });
  }

  Future<List<HealthCenter>> get() {
    return db
        .query('health_centers')
        .then((value) => value.map((e) => HealthCenter.fromJson(e)).toList());
  }

  @override
  Future<void> init() async {
    sqfliteFfiInit();

    var path = 'bs.db';

    late DatabaseFactory factory;

    if (kIsWeb) {
      factory = databaseFactoryFfiWeb;
    } else {
      factory = databaseFactoryFfi;

      path = join(await factory.getDatabasesPath(), path);
    }

    await factory.deleteDatabase(path);

    db = await factory.openDatabase(
      path,
      options: OpenDatabaseOptions(
        onCreate: (db, version) async {
          final batch = db.batch();

          batch.execute('''
CREATE TABLE health_centers (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  phone_number TEXT NOT NULL,
  city TEXT NOT NULL,
  address TEXT NOT NULL
);
          ''');

          await batch.commit();
        },
        version: 1,
      ),
    );
  }
}
