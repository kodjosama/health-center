import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

class NoConnectionDialog extends StatelessWidget {
  const NoConnectionDialog({
    super.key,
    required this.request,
    required this.completer,
  });

  final DialogRequest request;

  final void Function(DialogResponse) completer;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: AlertDialog(
        title: const Text('Impossible de se connecter'),
        content: const Row(
          children: [
            Icon(
              Icons.warning,
              size: 48.0,
              color: Color(0xffffe08a),
            ),

            // ...
            SizedBox(width: 16.0),
            Expanded(
              child: Text(
                'Vérifiez votre connexion internet. Si le problème persiste, veuillez réessayer dans un instant.',
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () => completer(DialogResponse(confirmed: true)),
            child: const Text('Réessayer'),
          ),
        ],
      ),
    );
  }
}
