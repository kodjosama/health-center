import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

class LoadingDialog extends StatelessWidget {
  const LoadingDialog({
    super.key,
    required this.request,
    required this.completer,
  });

  final DialogRequest request;

  final void Function(DialogResponse) completer;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: AlertDialog(
        content: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const CircularProgressIndicator(strokeWidth: 3.0),
              const SizedBox(width: 16.0),
              Expanded(child: Text(request.title ?? 'Chargement en cours...')),
            ],
          ),
        ),
        scrollable: true,
      ),
    );
  }
}
