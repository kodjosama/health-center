import 'package:flutter/material.dart';
import 'package:stacked_services/stacked_services.dart';

class ErrorOccurredDialog extends StatelessWidget {
  const ErrorOccurredDialog({
    super.key,
    required this.request,
    required this.completer,
  });

  final DialogRequest request;

  final void Function(DialogResponse) completer;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: AlertDialog(
        title: const Text('Oups'),
        content: const Row(
          children: [
            Icon(
              Icons.error,
              size: 48.0,
              color: Colors.red,
            ),
            SizedBox(width: 16.0),
            Expanded(child: Text('Désolé, une erreur est survenue')),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () => completer(DialogResponse(confirmed: true)),
            child: const Text('Réessayer'),
          ),
        ],
      ),
    );
  }
}
