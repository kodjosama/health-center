import 'package:hc/app/app.locator.dart';
import 'package:hc/models/health_center.dart';
import 'package:hc/services/api/health_center_api_service.dart';
import 'package:hc/services/db/health_center_db_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HealthCenterListViewModel extends FutureViewModel<List<HealthCenter>> {
  final dbService = locator<HealthCenterDbService>();
  final apiService = locator<HealthCenterApiService>();
  final navigationService = locator<NavigationService>();

  @override
  bool get rethrowException => true;

  @override
  Future<List<HealthCenter>> futureToRun() => apiService.get();
}
