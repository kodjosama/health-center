import 'package:flutter/material.dart';
import 'package:hc/app/app.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_hooks/stacked_hooks.dart';

import 'health_center_list_viewmodel.dart';

class HealthCenterListView extends StackedView<HealthCenterListViewModel> {
  const HealthCenterListView({super.key});

  @override
  Widget builder(
    BuildContext context,
    HealthCenterListViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Health Center'),
      ),
      body: const _List(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () =>
            viewModel.navigationService.navigateToHealthCenterFormView(),
        label: const Text('Nouveau centre'),
      ),
    );
  }

  @override
  HealthCenterListViewModel viewModelBuilder(BuildContext context) =>
      HealthCenterListViewModel();

  @override
  bool get reactive => false;
}

class _List extends StackedHookView<HealthCenterListViewModel> {
  const _List();

  @override
  Widget builder(BuildContext context, HealthCenterListViewModel viewModel) {
    if (viewModel.isBusy) {
      return const Center(child: Text('Chargement en cours...'));
    }

    if (viewModel.hasError) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text('Une erreur est survenue'),

            // ...
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: () => viewModel.initialise(),
              child: const Text('Rééssayer!'),
            ),
          ],
        ),
      );
    }

    if (viewModel.data == null || viewModel.data!.isEmpty) {
      return const Center(
        child: Text('Liste vide!'),
      );
    }

    return ListView.builder(
      itemBuilder: (context, index) {
        final item = viewModel.data![index];

        return ListTile(
          title: Text(item.name),
          trailing: const Icon(Icons.chevron_right),
          onTap: () => viewModel.navigationService
              .navigateToHealthCenterDetailsView(healthCenter: item),
        );
      },
      itemCount: viewModel.data!.length,
    );
  }
}
