import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:hc/app/app.locator.dart';
import 'package:hc/enums/dialog_type.dart';
import 'package:hc/enums/snackbar_type.dart';
import 'package:hc/models/health_center.dart';
import 'package:hc/services/api/health_center_api_service.dart';
import 'package:hc/services/db/health_center_db_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HealthCenterFormViewModel extends BaseViewModel {
  final dialogService = locator<DialogService>();
  final snackbarService = locator<SnackbarService>();
  final dbService = locator<HealthCenterDbService>();
  final apiService = locator<HealthCenterApiService>();
  final navigationService = locator<NavigationService>();

  final formKey = GlobalKey<FormState>();

  int? id;

  String? name;

  String? phoneNumber;

  String? city;

  String? address;

  bool submitting = false;

  Map<String, dynamic> errors = {};

  HealthCenterFormViewModel(HealthCenter? healthCenter) {
    id = healthCenter?.id;

    name = healthCenter?.name;

    phoneNumber = healthCenter?.phoneNumber;

    city = healthCenter?.city;

    address = healthCenter?.address;
  }

  void closeLoadingDialog() {
    submitting = false;

    rebuildUi();

    dialogService.completeDialog(DialogResponse());
  }

  void setAddress(String? address) {
    this.address = address;

    rebuildUi();
  }

  void setCity(String? city) {
    this.city = city;

    rebuildUi();
  }

  void setName(String? name) {
    this.name = name;

    rebuildUi();
  }

  void setPhoneNumber(String? phoneNumber) {
    this.phoneNumber = phoneNumber;

    rebuildUi();
  }

  void showLoadingDialog() {
    errors = {};

    submitting = true;

    rebuildUi();

    dialogService.showCustomDialog(variant: DialogType.loading);
  }

  Future<void> submit() async {
    try {
      showLoadingDialog();

      final data = {
        'name': name,
        'phone_number': phoneNumber,
        'city': city,
        'address': address,
      };

      if (id == null) {
        await apiService.create(FormData.fromMap(data));
      } else {
        await dbService.create({...data, 'id': id});
      }

      closeLoadingDialog();

      snackbarService.showCustomSnackBar(
        message: '👌👌👌',
        variant: SnackbarType.success,
      );
    } on DioException catch (error) {
      closeLoadingDialog();

      if (error.response != null && error.response!.statusCode == 422) {
        errors = (error.response!.data['errors'] as Map)
            .map((key, value) => MapEntry(key, value[0]));

        rebuildUi();

        return;
      }

      if (error.type == DioExceptionType.connectionTimeout ||
          error.type == DioExceptionType.sendTimeout ||
          error.type == DioExceptionType.receiveTimeout) {
        dialogService.showCustomDialog(variant: DialogType.noConnection);

        return;
      }

      dialogService.showCustomDialog(variant: DialogType.errorOccurred);
    } catch (error) {
      closeLoadingDialog();

      dialogService.showCustomDialog(variant: DialogType.errorOccurred);
    }
  }
}
