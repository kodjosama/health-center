import 'package:flutter/material.dart';
import 'package:hc/models/health_center.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_hooks/stacked_hooks.dart';

import 'health_center_form_viewmodel.dart';

class HealthCenterFormView extends StackedView<HealthCenterFormViewModel> {
  final HealthCenter? healthCenter;

  const HealthCenterFormView({super.key, this.healthCenter});

  @override
  Widget builder(
    BuildContext context,
    HealthCenterFormViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: healthCenter == null
            ? const Text('Nouveau centre')
            : const Text('Modifier centre'),
      ),
      body: const _Form(),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
          onPressed: viewModel.submitting
              ? null
              : () {
                  if (viewModel.formKey.currentState!.validate()) {
                    viewModel.submit();
                  }
                },
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Colors.deepPurple,
            padding: const EdgeInsets.symmetric(
              vertical: 24.0,
              horizontal: 32.0,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4.0),
            ),
          ),
          child: Consumer<HealthCenterFormViewModel>(
            builder: (context, viewModel, child) {
              if (viewModel.submitting) {
                return const SizedBox.square(
                  dimension: 16.0,
                  child: CircularProgressIndicator(
                    strokeWidth: 2.0,
                  ),
                );
              }

              return const Text('Enregistrer');
            },
          ),
        ),
      ),
    );
  }

  @override
  HealthCenterFormViewModel viewModelBuilder(BuildContext context) =>
      HealthCenterFormViewModel(healthCenter);

  @override
  bool get reactive => false;
}

class _Form extends StackedHookView<HealthCenterFormViewModel> {
  const _Form();

  @override
  Widget builder(BuildContext context, HealthCenterFormViewModel viewModel) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: viewModel.formKey,
        child: Column(
          children: [
            TextFormField(
              initialValue: viewModel.name,
              decoration: InputDecoration(
                labelText: 'Désignation',
                hintText: 'Désignation',
                errorText: viewModel.errors['name'],
              ),
              keyboardType: TextInputType.name,
              onChanged: (value) => viewModel.setName(value),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'la désignation est obligatoire';
                }

                return null;
              },
            ),

            // ...
            const SizedBox(height: 16.0),
            TextFormField(
              initialValue: viewModel.phoneNumber,
              decoration: InputDecoration(
                labelText: 'Numéro de tépéhone',
                hintText: 'Numéro de tépéhone',
                errorText: viewModel.errors['phone_number'],
              ),
              keyboardType: TextInputType.phone,
              onChanged: (value) => viewModel.setPhoneNumber(value),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'le numéro de téléphone est obligatoire';
                }

                return null;
              },
            ),

            // ...
            const SizedBox(height: 16.0),
            TextFormField(
              initialValue: viewModel.city,
              decoration: InputDecoration(
                labelText: 'Ville',
                hintText: 'Ville',
                errorText: viewModel.errors['city'],
              ),
              keyboardType: TextInputType.name,
              onChanged: (value) => viewModel.setCity(value),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'la ville est obligatoire';
                }

                return null;
              },
            ),

            // ...
            const SizedBox(height: 16.0),
            TextFormField(
              initialValue: viewModel.address,
              decoration: InputDecoration(
                labelText: 'Adresse',
                hintText: 'Adresse',
                errorText: viewModel.errors['address'],
              ),
              keyboardType: TextInputType.multiline,
              maxLines: 2,
              onChanged: (value) => viewModel.setAddress(value),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'l\'adresse est obligatoire';
                }

                return null;
              },
            ),
          ],
        ),
      ),
    );
  }
}
