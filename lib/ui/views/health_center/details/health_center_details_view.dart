import 'package:flutter/material.dart';
import 'package:hc/app/app.router.dart';
import 'package:hc/models/health_center.dart';
import 'package:stacked/stacked.dart';

import 'health_center_details_viewmodel.dart';

class HealthCenterDetailsView
    extends StackedView<HealthCenterDetailsViewModel> {
  final HealthCenter healthCenter;

  const HealthCenterDetailsView({super.key, required this.healthCenter});

  @override
  Widget builder(
    BuildContext context,
    HealthCenterDetailsViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      appBar: AppBar(title: const Text('Details')),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Désignation',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 8.0),
            Text(healthCenter.name),

            // ...
            const SizedBox(height: 24.0),
            Text(
              'Numéro de téléphone',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 8.0),
            Text(healthCenter.phoneNumber),

            // ...
            const SizedBox(height: 24.0),
            Text(
              'Ville',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 8.0),
            Text(healthCenter.city),

            // ...
            const SizedBox(height: 24.0),
            Text(
              'Adresse',
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 8.0),
            Text(healthCenter.address),

            // ...
            const SizedBox(height: 24.0),
            TextButton(
              onPressed: () => viewModel.navigationService
                  .navigateToHealthCenterFormView(healthCenter: healthCenter),
              child: const Text('Modifier'),
            ),
          ],
        ),
      ),
    );
  }

  @override
  HealthCenterDetailsViewModel viewModelBuilder(BuildContext context) =>
      HealthCenterDetailsViewModel();
}
