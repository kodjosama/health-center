import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:hc/app/app.locator.dart';
import 'package:hc/services/connectivity_service.dart';

class ConnectivityViewModel with ChangeNotifier {
  ConnectivityResult state = ConnectivityResult.none;

  final ConnectivityService connectivityService =
      locator<ConnectivityService>();

  late StreamSubscription<ConnectivityResult> subscription;

  ConnectivityViewModel() {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      debugPrint('connectivity state changed $result');

      state = result;

      connectivityService.state = result;

      notifyListeners();
    });
  }

  @override
  void dispose() {
    subscription.cancel();

    super.dispose();
  }
}
