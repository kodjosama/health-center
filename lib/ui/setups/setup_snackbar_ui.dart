import 'package:flutter/material.dart';
import 'package:hc/app/app.locator.dart';
import 'package:hc/enums/snackbar_type.dart';
import 'package:stacked_services/stacked_services.dart';

void setupSnackbarUi() {
  final snackbarService = locator<SnackbarService>();

  snackbarService.registerCustomSnackbarConfig(
    variant: SnackbarType.success,
    config: SnackbarConfig(
      textColor: Colors.white,
      margin: const EdgeInsets.all(24.0),
      padding: const EdgeInsets.all(16),
      borderRadius: 4.0,
      backgroundColor: const Color(0xff48c78e),
      // animationDuration: const Duration(seconds: 8),
    ),
  );
}
