import 'package:hc/app/app.locator.dart';
import 'package:hc/enums/dialog_type.dart';
import 'package:hc/ui/dialogs/error_occurred_dialog.dart';
import 'package:hc/ui/dialogs/loading_dialog.dart';
import 'package:hc/ui/dialogs/no_connection_dialog.dart';
import 'package:stacked_services/stacked_services.dart';

void setupDialogUi() {
  final dialogService = locator<DialogService>();

  Map<dynamic, DialogBuilder> builders = {
    DialogType.loading: (_, request, completer) =>
        LoadingDialog(request: request, completer: completer),
    DialogType.noConnection: (_, request, completer) =>
        NoConnectionDialog(request: request, completer: completer),
    DialogType.errorOccurred: (_, request, completer) =>
        ErrorOccurredDialog(request: request, completer: completer),
  };

  dialogService.registerCustomDialogBuilders(builders);
}
